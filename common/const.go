package common

const (
	//Header names
	Accept                   = "Accept"
	ContentType              = "Content-type"
	ApplicationJSON          = "application/json"
	AccessControlAllowOrigin = "Access-Control-Allow-Origin"
	Asterisk                 = "*"

	// URL Params
	URLQueryParameterUserId = "userId"
	URLQueryParameterDictionaryId = "dictionaryId"
	URLQueryParameterTableName = "tableName"
)
