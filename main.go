package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"gitlab.com/Varvara_R/talkilet-be/handler"
	service "gitlab.com/Varvara_R/talkilet-be/service"
	"gitlab.com/Varvara_R/talkilet-be/utils"
)

func main() {
	// api
	router := httprouter.New()
	// router.GET("/api/user", handler.GetUser)
	// router.POST("/api/user", handler.CreateUser)

	router.GET("/api/dictionary", handler.GetDictionary)
	router.POST("/api/dictionary", handler.CreateDictionary)
	router.DELETE("/api/dictionary", handler.DeleteDictionary)

	// router.GET("/api/words", handler.GetWords)
	router.POST("/api/words", handler.CreateWords)
	router.PATCH("/api/words", handler.UpdateWords)
	router.DELETE("/api/words", handler.DeleteWords)

	router.POST("/api/events", handler.PostEvents)
	router.GET("/metrics", handler.GetMetrics)

	// router.POST("/api/word", UpdateWord)

	// metrics registration
	utils.RegisterMetrics()

	// db starts
	service.DbInitiate()





	// server serves
	port := ":8080"
	logrus.Printf("Server has started successfully, listening on %s \n", port)
	http.ListenAndServe(port, router)



}
