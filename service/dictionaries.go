package service

import (
	"fmt"
	"strconv"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/Varvara_R/talkilet-be/api/incoming"
	utils "gitlab.com/Varvara_R/talkilet-be/utils/helpers"
)

type Dictionary struct {
	Id            string `gorm:"primaryKey"`
	Name          string `gorm:"name"`
	Language_from string `gorm:"language_from"`
	Language_to   string `gorm:"language_to"`
	Words         []Word `gorm:"foreignKey:DictionaryID"`
}

func GetAllDictionaries() ([]*Dictionary, bool) {
	dictionaries := make([]*Dictionary, 0)
	err := GetDB().Table("dictionaries").Find(&dictionaries).Error
	if err != nil {
		logrus.Error(err)
		return nil, false
	}

	return dictionaries, true
}

func (d *Dictionary) Validate() (map[string]interface{}, bool) {
	if d.Name == "" {
		return utils.Message(false, "Dictionary name cannot be empty"), false
	}

	if d.Language_from == "" {
		return utils.Message(false, "Dictionary Language_from cannot be empty"), false
	}

	if d.Language_to == "" {
		return utils.Message(false, "Dictionary Language_to cannot be empty"), false
	}

	//All the required parameters are present
	return utils.Message(true, "Dictionary body validated successfully"), true
}

func (d *Dictionary) Create() error {
	if resp, ok := d.Validate(); !ok {
		err := fmt.Errorf("Dictionary validation error: %s", resp["message"])
		logrus.Error(err)
		return err
	}

	result := GetDB().Create(d)
	if result.Error != nil {
		logrus.Error(result.Error)
		return result.Error
	}
	logrus.Info(fmt.Sprintf("Dictionary created successfully - %s %s", d.Id, d.Name))
	return nil
}

func Delete(id string) error {
	err := GetDB().Where("id = ?", id).Delete(&Dictionary{}).Error
	if err != nil {
		logrus.Error(err)
		return err
	}
	logrus.Info(fmt.Sprintf("Dictionary deleted successfully - %s", id))
	return nil
}

func GetDictionaryById(id string) (*Dictionary, error) {
	var dictionary *Dictionary
	err := db.Model(&Dictionary{}).Where("id = ?", id).Preload("Words").First(&dictionary).Error
	if err != nil {
		err := fmt.Errorf("GetDictionaryById from DB returns error - %s", err)
		logrus.Error(err)
		return nil, err
	}
	logrus.Info(fmt.Sprintf("DictionaryById found successfully - %s", id))

	return dictionary, nil
}

func MapDictionaryForDb(incDictionary *incoming.DictionaryRequestBody) *Dictionary {
	mappedDictionary := &Dictionary{
		Id:            getValidDictionaryId(),
		Name:          incDictionary.Name,
		Language_from: incDictionary.LanguageFrom,
		Language_to:   incDictionary.LanguageTo,
	}
	return mappedDictionary
}

// private methods
func createDictionaryId() string {
	uniqueID := uuid.New().ID()
	return strconv.FormatInt(int64(uniqueID), 10)
}

func getValidDictionaryId() string {
	var dictionaryId string
	whileIdDoesNotExist := true
	for whileIdDoesNotExist {
		dictionaryId = createDictionaryId()
		err := GetDB().Where("id = ?", dictionaryId).First(&Dictionary{}).Error
		logrus.Debugf("for dictionary_id - %s - %s ", dictionaryId, err)
		logrus.Info("ERR in create dict id", err)
		if err != nil { // means no records found - err
			logrus.Info("in err handle for dict id create")
			whileIdDoesNotExist = false
		}
	}
	return dictionaryId
}
