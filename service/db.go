package service

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

func DbInitiate() {

	err := godotenv.Load()
	if err != nil {
		logrus.Error(err)
	}

	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")

	dbUri := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, username, dbName, password) //Создать строку подключения

	db, err = gorm.Open(postgres.Open(dbUri), &gorm.Config{})
	if err != nil {
		fmt.Print(err)
	}

	db.Debug().AutoMigrate(&Dictionary{}, &Word{}) // DB migration
	// sample1 := Dictionary{
	// 	Id:            "1122",
	// 	Name:          "English",
	// 	Language_from: "HU",
	// 	Language_to:   "RU",
	// 	Words: []Word{
	// 		{Word_id: "A", Language_code: "HU", Translation: "hogy vagy"},
	// 		{Word_id: "B", Language_code: "HU", Translation: "szia"},
	// 	},
	// }

	// db.Create(&sample1)

	// var test Dictionary

	// db.Model(&Dictionary{}).Where("id = ?", "1122").Preload("Words").First(&test)
	// logrus.Info("TEST", test)
	

}

func GetDB() *gorm.DB {
	return db
}
