package service

import (
	"errors"

	"github.com/sirupsen/logrus"
	"gitlab.com/Varvara_R/talkilet-be/api/incoming"
	utils "gitlab.com/Varvara_R/talkilet-be/utils/helpers"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username       string       `json:"username"`
	FirstName      string       `json:"firstName"`
	LastName       string       `json:"lastName"`
	Email          string       `json:"email"`
	Dictionaries   []Dictionary `json:"dictionaries,omitempty"`
	Score          int          `json:"score,omitempty"`
	Current_strike int          `json:"current_strike,omitempty"`
	Best_strike    int          `json:"best_strike,omitempty"`
}

func (u *User) Validate() (map[string]interface{}, bool) {
	if u.Username == "" {
		return utils.Message(false, "Username cannot be empty"), false
	}

	if u.FirstName == "" {
		return utils.Message(false, "User validate: Firstname should be specified"), false
	}

	if u.LastName == "" {
		return utils.Message(false, "User validate: Lastname should be specified"), false
	}

	if u.Email == "" {
		return utils.Message(false, "User validate: E-mail should be specified"), false
	}

	//All the required parameters are present
	return utils.Message(true, "success"), true
}

func (u *User) Create() (map[string]interface{}, error) {
	if resp, ok := u.Validate(); !ok {
		err := errors.New("User validation error")
		logrus.Error(err)
		return resp, err
	}

	// check if such email already exist
	user := &User{}
	err := GetDB().Table("users").Where("email = ?", u.Email).First(user).Error
	if err == nil {
		err = errors.New("User email already exists ")
		logrus.Error(err)
		return nil, err
	}

	GetDB().Create(u)

	resp := utils.Message(true, "success")
	resp["user"] = u
	// logrus.Info("RESP CREATE ", resp)
	return resp, nil
}

func GetUser(id string) (*User, error) {
	user := &User{}
	err := GetDB().Table("users").Where("id = ?", id).First(user).Error

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			logrus.Error("user id is not found ", err)
			return user, err
		}
		logrus.Error("connection error. Please retry", err)
		return user, err
	}

	return user, nil
}

func MapUserBody(incUser *incoming.UserRequestBody) *User {
	user := &User{
		Username:       incUser.Username,
		FirstName:      incUser.FirstName,
		LastName:       incUser.LastName,
		Email:          incUser.Email,
		Score:          0,
		Current_strike: 0,
		Best_strike:    0,
	}
	return user
}
