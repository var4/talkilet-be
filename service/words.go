package service

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"math/big"

	"github.com/sirupsen/logrus"
	"gitlab.com/Varvara_R/talkilet-be/api/incoming"
	utils "gitlab.com/Varvara_R/talkilet-be/utils/helpers"
)

type Word struct {
	Word_id       string `gorm:"primaryKey"`
	Language_code string `gorm:"primaryKey"`
	Translation   string `gorm:"translation"`
	DictionaryID  string
}

// func GetAllWords() ([]*Word, bool) {
// 	words := make([]*Word, 0)
// 	err := GetDB().Table("words").Order("word_id").Find(&words).Error
// 	if err != nil {
// 		logrus.Error(err)
// 		return nil, false
// 	}

// 	return words, true
// }

func (word *Word) Validate() (map[string]interface{}, bool) {
	if word.Translation == "" {
		return utils.Message(false, "Translation cannot be empty"), false
	}
	if word.Language_code == "" {
		return utils.Message(false, fmt.Sprintf("LanguageCode of %s cannot be empty", word.Translation)), false
	}

	//All the required parameters are present
	return utils.Message(true, fmt.Sprintf("Word validation passes successfully for %s", word.Translation)), true
}

func (word *Word) Create() (map[string]interface{}, bool) {
	if resp, ok := word.Validate(); !ok {
		return resp, false
	}

	GetDB().Create(word)
	resp := utils.Message(true, fmt.Sprintf("Word created successfully - %s %s", word.Translation, word.Word_id))
	return resp, true
}

func (word *Word) Delete() (map[string]interface{}, bool) {
	err := GetDB().Where("word_id = ? AND language_code = ?", word.Word_id, word.Language_code).Delete(&Word{}).Error
	if err != nil {
		logrus.Error(err)
		return nil, false
	}
	resp := utils.Message(true, fmt.Sprintf("Word deleted successfully - %s, %s", word.Translation, word.Word_id))
	return resp, true
}

// new pairs of words
func MapNewWords(words []incoming.NewWordFromUI) []Word {
	var mappedWords []Word
	for _, item := range words {
		var wordId string
		whileIdDoesNotExist := true
		for whileIdDoesNotExist {
			wordId = createWordId()
			err := GetDB().Where("word_id = ?", wordId).First(&Word{}).Error
			logrus.Debugf("for word_id - %s - %s ", wordId, err)
			if err != nil {
				whileIdDoesNotExist = false
			}
		}
		wordHU := Word{
			Word_id:       wordId,
			Language_code: "HU",
			Translation:   item.Translation,
		}
		wordRU := Word{
			Word_id:       wordId,
			Language_code: "RU",
			Translation:   item.Translation__1,
		}
		mappedWords = append(mappedWords, wordHU, wordRU)
	}
	return mappedWords
}

// mappers for DB and Ui structs
func MapWordToDBModel(incWord *incoming.UpdateWordFromUI) *Word {
	return &Word{
		Word_id:       incWord.WordId,
		Language_code: incWord.LanguageCode,
		Translation:   incWord.Translation,
	}
}

func MapWordsToUi(words []*Word) []incoming.UiWord {
	var mappedWords []incoming.UiWord
	for _, word := range words {
		mappedWord := incoming.UiWord{
			WordId:       word.Word_id,
			LanguageCode: word.Language_code,
			Translation:  word.Translation,
		}
		mappedWords = append(mappedWords, mappedWord)
	}
	return mappedWords
}

// private methods
func createWordId() string {
	// Generate a 64 bit number
	val, _ := randint64()

	// Encode the 64 bit number
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(val))

	id := base64.RawURLEncoding.EncodeToString(b)
	return id
}

const min = 100000000
const max = 100000000000

func randint64() (int64, error) {
	val, err := rand.Int(rand.Reader, big.NewInt(max))
	// val, err := rand.Int(rand.Reader, big.NewInt(int64(math.MaxInt64)))
	if err != nil {
		return min, err
	}
	return val.Int64(), nil
}
