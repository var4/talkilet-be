package incoming

type Dictionary struct {
	DictionaryId string `json:"dictionaryId"`
	Name         string `json:"name"`
	// Words        []Word `json:"words"` //pass only total qty of words?
	LanguageFrom string `json:"languageFrom"`
	LanguageTo   string `json:"languageTo"`
}

type DictionaryRequestBody struct {
	Name         string `json:"name"`
	LanguageFrom string `json:"languageFrom"`
	LanguageTo   string `json:"languageTo"`
}
