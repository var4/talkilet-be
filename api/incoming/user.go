package incoming

type User struct {
	ID            string       `json:"ID"`
	Username      string       `json:"username"`
	FirstName     string       `json:"firstName"`
	LastName      string       `json:"lastName"`
	Email         string       `json:"email"`
	Dictionaries  []Dictionary `json:"dictionaries"`
	Score         int          `json:"score,omitempty"`
	CurrentStrike int          `json:"currentStrike,omitempty"`
	BestStrike    int          `json:"bestStrike,omitempty"`
	CreatedOn     string       `json:"createdOn"`
}

// INSERT INTO users (username, first_name, last_name, email, score, current_strike, best_strike, created_on) VALUES ('Viktoria', 'Viktoria', 'The Troll', 'viktoria@test.com', 120, 1, 10, NOW())

type UserRequestBody struct {
	Username  string `json:"username"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"` 
}
