package incoming

type UiWord struct {
	WordId       string `json:"wordId"`
	LanguageCode string `json:"languageCode"`
	Translation  string `json:"translation"`
}

type UpdateWordFromUI struct {
	Translation  string `json:"translation"`
	LanguageCode string `json:"languageCode"`
	WordId       string `json:"wordId"`
}

type NewWordFromUI struct {
	Translation    string `json:"translation"`
	Translation__1 string `json:"translation__1"`
	Chapter        string `json:"chapter,omitempty"`
}
