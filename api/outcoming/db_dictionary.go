package outcoming

type Db_dictionary struct {
	Id            string `json:"id"` // unique
	Name          string `json:"name"`
	Language_from string `json:"language_from"`
	Language_to   string `json:"language_to"`
}
type Db_dictionary_words struct {
	Word_id       string `json:"word_id"`
	Dictionary_id string `json:"dictionary_id"`
}
