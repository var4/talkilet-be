package outcoming

type Word struct {
	Id            string `json:"id"`
	Word_id       string `json:"word_id"`
	Language_code string `json:"language_code"`
	Translation   string `json:"translation"`
}
