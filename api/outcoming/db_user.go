package outcoming

type User struct {
	User_id        string `json:"user_id"`
	Username       string `json:"username"`
	First_Name     string `json:"first_name"`
	Last_Name      string `json:"last_name"`
	Email          string `json:"email"`
	Score          int    `json:"score,omitempty"`
	Current_strike int    `json:"current_strike,omitempty"`
	Best_strike    int    `json:"best_strike,omitempty"`
	Created_on     string `json:"created_on"`
}
