package utils

import "gitlab.com/Varvara_R/talkilet-be/utils/metrics"

func RegisterMetrics() {
	metrics.RegisterBeMetrics()
	metrics.RegisterUIMetrics()
}