package metrics

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

type beMetric struct {
	name                 string
	requestTime          prometheus.Gauge
	requestCounter       prometheus.Counter
	errorResponseCounter *prometheus.CounterVec
}

func creatMetrics(key, name, method string) beMetric {
	requestTime := prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("request_time_%s", name),
		Help:      fmt.Sprintf("Duration of %s request to %s, ms", method, key),
	})

	requestCounter := prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("request_counter_%s", name),
		Help:      fmt.Sprintf("Request counter of %s request to %s, times requested", method, key),
	})

	errorResponseCounter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Subsystem: "be",
		Name:      fmt.Sprintf("error_response_counter_%s", name),
		Help:      fmt.Sprintf("Error response counter of %s request to %s, times got", method, key),
	}, []string{"code"})

	return beMetric{
		name:                 name,
		requestTime:          requestTime,
		requestCounter:       requestCounter,
		errorResponseCounter: errorResponseCounter,
	}
}

func (m *beMetric) register() {
	if err := prometheus.Register(m.requestTime); err != nil {
		logrus.WithError(err).Fatalf("Unable to register be_request_time_%s", m.name)
	} else {
		logrus.Debug("be_request_time_%s gauge registered successfully", m.name)
	}

	if err := prometheus.Register(m.requestCounter); err != nil {
		logrus.WithError(err).Fatalf("Unable to register be_request_counter_%s", m.name)
	} else {
		logrus.Debug("be_request_counter_%s counter registered successfully", m.name)
	}

	if err := prometheus.Register(m.errorResponseCounter); err != nil {
		logrus.WithError(err).Fatalf("Unable to register be_error_response_counter_%s", m.name)
	} else {
		logrus.Debug("be_error_response_counter_%s error response registered successfully", m.name)
	}
}

const (
	BERequestMetricsUserRouteKey       = "/api/user"
	BERequestMetricsDictionaryRouteKey = "/api/dictionary"
	BERequestMetricsWordsRouteKey      = "/api/words"

	BERequestGetMetricsRouteKey = "/api/metrics"
	BERequestPostEventsRouteKey = "/api/events"
)

var (
	beGetUser    = creatMetrics(BERequestMetricsUserRouteKey, "get_user", http.MethodGet)
	beCreateUser = creatMetrics(BERequestMetricsUserRouteKey, "post_new_user", http.MethodPost)

	beGetDictionary  = creatMetrics(BERequestMetricsDictionaryRouteKey, "get_dictionary", http.MethodGet)
	bePostDictionary = creatMetrics(BERequestMetricsDictionaryRouteKey, "post_new_dictionary", http.MethodPost)

	beGetWords     = creatMetrics(BERequestMetricsWordsRouteKey, "get_words", http.MethodGet)
	bePostNewWords = creatMetrics(BERequestMetricsWordsRouteKey, "post_new_words", http.MethodPost)
	bePatchWords = creatMetrics(BERequestMetricsWordsRouteKey, "patch_words", http.MethodPatch)
	beDeleteWords = creatMetrics(BERequestMetricsWordsRouteKey, "delete_words", http.MethodDelete)

	beGetMetrics = creatMetrics(BERequestGetMetricsRouteKey, "get_metrics", http.MethodGet)
	bePostEvents = creatMetrics(BERequestPostEventsRouteKey, "post_events", http.MethodPost)
)

func RegisterBeMetrics() {
	beGetUser.register()
	beCreateUser.register()
	beGetDictionary.register()
	bePostDictionary.register()
	beGetWords.register()
	bePostNewWords.register()
	bePatchWords.register()
	beDeleteWords.register()
	beGetMetrics.register()
	bePostEvents.register()
}

func selectMetrics(key string, methodOptionalValue ...string) *beMetric {
	method := http.MethodGet
	if len(methodOptionalValue) > 0 && methodOptionalValue[0] == http.MethodPost {
		// logrus.Info("methodOptionalValue ", methodOptionalValue)
		method = http.MethodPost
	}

	switch key {
	case BERequestMetricsUserRouteKey:
		if method == http.MethodGet {
			return &beGetUser
		} else if method == http.MethodPost {
			return &beCreateUser
		}

	case BERequestMetricsDictionaryRouteKey:
		if method == http.MethodGet {
			return &beGetDictionary
		} else if method == http.MethodPost {
			return &bePostDictionary
		}

	case BERequestMetricsWordsRouteKey:
		if method == http.MethodGet {
			return &beGetWords
		} else if method == http.MethodPost {
			return &bePostNewWords
		} else if method == http.MethodPatch {
			return &bePatchWords
		} else if method == http.MethodDelete {
			return &beDeleteWords
		}

	case BERequestGetMetricsRouteKey:
		return &beGetMetrics
	case BERequestPostEventsRouteKey:
		return &bePostEvents
	default:
		logrus.Warnf("No metric for the route method and key found: %s %s", method, key)
	}
	return nil
}

func SendDuration(key string, start time.Time, methodOptionalValue ...string) {
	duration := time.Now().Sub(start)
	metrics := selectMetrics(key, methodOptionalValue...)
	if metrics != nil {
		metrics.requestTime.Set(duration.Seconds() * 1000)
	}
}

func IncrementRequestCounter(key string, methodOptionalValue ...string) {
	metrics := selectMetrics(key, methodOptionalValue...)
	if metrics != nil {
		metrics.requestCounter.Inc()
	}
}

func IncrementErrorResponseCounter(key string, code int, methodOptionalValue ...string) {
	metrics := selectMetrics(key, methodOptionalValue...)
	if metrics != nil {
		metrics.errorResponseCounter.WithLabelValues(strconv.Itoa(code)).Inc()
	}
}
