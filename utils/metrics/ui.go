package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
)

type UIEvent struct {
	Metric UIEventMetric `json:"metric"`
	Log    UIEventLog    `json:"log"`
}

type UIEventMetric struct {
	Name   string              `json:"name"`
	Labels UIEventMetricLabels `json:"labels"`
	Value  float64             `json:"value"`
}

type UIEventMetricLabels struct {
	Vital string `json:"vital"`
}

type UIEventLog struct {
	Level   string `json:"level"`
	Message string `json:"message"`
}

func RegisterUIMetrics() {
	if err := prometheus.Register(UIPageLoads); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_page_loads")
	} else {
		logrus.Debug("ui_page_loads counter registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsLCP); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_lcp")
	} else {
		logrus.Debug("ui_web_vitals_lcp gauge registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsFID); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_fid")
	} else {
		logrus.Debug("ui_web_vitals_fid gauge registered successfully")
	}

	if err := prometheus.Register(UIWebVitalsCLS); err != nil {
		logrus.WithError(err).Fatal("Unable to register ui_web_vitals_cls")
	} else {
		logrus.Debug("ui_web_vitals_cls gauge registered successfully")
	}
}

var (
	UIPageLoads = prometheus.NewCounter(prometheus.CounterOpts{
		Subsystem: "ui",
		Name: "ui_page_loads",
		Help: "Amount of page loads",
	})

	UIWebVitalsLCP = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name: "web_vitals_lcp",
		Help: "Largest Contentful Paint (LCP): measurs loading performance",
	})

	UIWebVitalsFID = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name: "web_vitals_fid",
		Help: "First input delay (FID): measurs loading performance",
	})

	UIWebVitalsCLS = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "ui",
		Name: "web_vitals_cls",
		Help: "Cumulative Layout Shift (CLS): measurs loading performance",
	})
)

const (
	uiPageLoadsMetric = "ui_page_loads"
	uiWebVitalsMetric = "ui_web_vitals"

	uiWebVitalsLabelLCP = "lcp"
	uiWebVitalsLabelFID = "fid"
	uiWebVitalsLabelCLS = "cls"
)

func SendUIEvent(event UIEvent) error {
	switch event.Metric.Name {
	case uiPageLoadsMetric:
		UIPageLoads.Add(event.Metric.Value)
		logrus.Info("Page loaded")
	case uiWebVitalsMetric:
		switch event.Metric.Labels.Vital {
		case uiWebVitalsLabelLCP:
			UIWebVitalsLCP.Set(event.Metric.Value)
		case uiWebVitalsLabelFID:
			UIWebVitalsFID.Set(event.Metric.Value)
		case uiWebVitalsLabelCLS:
			UIWebVitalsCLS.Set(event.Metric.Value)
		}
	}
	return nil
}