package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/Varvara_R/talkilet-be/api/incoming"
	"gitlab.com/Varvara_R/talkilet-be/common"
	"gitlab.com/Varvara_R/talkilet-be/service"

	"gitlab.com/Varvara_R/talkilet-be/utils/metrics"
)

// func GetUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
// 	beRequestMetricKey := metrics.BERequestMetricsUserRouteKey
// 	defer metrics.SendDuration(beRequestMetricKey, time.Now())
// 	metrics.IncrementRequestCounter(beRequestMetricKey)

// 	id := r.URL.Query().Get(common.URLQueryParameterUserId)
// 	resp, err := service.GetUser(id)

// 	if err != nil {
// 		logrus.Error(err, "Unable to get user by id")
// 		http.Error(w, "Unable to get user by id", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	result, err := json.Marshal(resp)

// 	if err != nil {
// 		logrus.Error(err, "Unable to marshal get user by id response")
// 		http.Error(w, "Unable to marshal get userby id  response", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set(common.ContentType, common.ApplicationJSON)
// 	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
// 	w.WriteHeader(http.StatusOK)
// 	_, err = fmt.Fprintf(w, "%s", result)

// 	if err != nil {
// 		logrus.Error(err, "Unable to write get user by id response in response writer")
// 		http.Error(w, "Unable to write get user by id response in response writer", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}
// }

// func CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
// 	beRequestMetricKey := metrics.BERequestMetricsUserRouteKey
// 	defer metrics.SendDuration(beRequestMetricKey, time.Now())
// 	metrics.IncrementRequestCounter(beRequestMetricKey)

// 	contentType := r.Header.Get(common.ContentType)
// 	if contentType != common.ApplicationJSON {
// 		err := errors.New("unsupported media type of create user")
// 		logrus.Error(err)
// 		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
// 		return
// 	}

// 	body, err := io.ReadAll(r.Body)
// 	if err != nil {
// 		logrus.Error(err, "Unable to read create user request body")
// 		http.Error(w, "Unable to read create user request body", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	var bodyUser incoming.UserRequestBody
// 	err = json.Unmarshal(body, &bodyUser)
// 	if err != nil {
// 		logrus.Error(err, "Unable to unmarshal create user request body")
// 		http.Error(w, "Unable to unmarshal create user request body", http.StatusBadRequest)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
// 		return
// 	}
// 	user := service.MapUserBody(&bodyUser)
// 	_, err = user.Create()
// 	if err != nil {
// 		logrus.Error(err, "Unable to create user")
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	result, err := json.Marshal(user)
// 	if err != nil {
// 		logrus.Error(err, "Unable to marshal create user response")
// 		http.Error(w, "Unable to marshal create user response", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set(common.ContentType, common.ApplicationJSON)
// 	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
// 	w.WriteHeader(http.StatusOK)
// 	_, _ = fmt.Fprintf(w, "%s", result)
// }

func GetDictionary(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsDictionaryRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	dictionary_id := r.URL.Query().Get(common.URLQueryParameterDictionaryId)
	dictionary, err := service.GetDictionaryById(dictionary_id) // can be empty []
	if err != nil {
		logrus.Error(err, "Unable to GetDictionaryById from DB")
		http.Error(w, "Unable to GetDictionaryById from DB", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}
	result, err := json.Marshal(dictionary)

	if err != nil {
		logrus.Error(err, "Unable to marshal get dictionaryById response")
		http.Error(w, "Unable to marshal get dictionarByIdy response", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprintf(w, "%s", result)

	if err != nil {
		logrus.Error(err, "Unable to write get dictionaryById response in response writer")
		http.Error(w, "Unable to write get dictionaryById response in response writer", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}
}

func CreateDictionary(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsDictionaryRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("unsupported media type of create dictionary")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error(err, "Unable to read create dictionary request body")
		http.Error(w, "Unable to read create dictionary request body", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	var bodyDictionary incoming.DictionaryRequestBody
	err = json.Unmarshal(body, &bodyDictionary)
	if err != nil {
		logrus.Error(err, "Unable to unmarshal create dictionary request body")
		http.Error(w, "Unable to unmarshal create dictionary request body", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	dictionary := service.MapDictionaryForDb(&bodyDictionary)
	err = dictionary.Create()
	if err != nil {
		logrus.Error("Unable to create dictionary ", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	// result, err := json.Marshal(dictionary)
	// if err != nil {
	// 	logrus.Error(err, "Unable to marshal post dictionary response")
	// 	http.Error(w, "Unable to marshal post dictionary response", http.StatusInternalServerError)
	// 	metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
	// 	return
	// }

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
	// _, _ = fmt.Fprintf(w, "%s", result)
}

func DeleteDictionary(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsDictionaryRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	dictionaryId := r.URL.Query().Get(common.URLQueryParameterDictionaryId)
	if len(dictionaryId) == 0 {
		logrus.Error("Param dictionaryId cannot be empty")
		http.Error(w, "Param dictionaryId cannot be empty", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	err := service.Delete(dictionaryId)
	if err != nil {
		logrus.Error("Unable to delete dictionary ", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
}

// func GetWords(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
// 	beRequestMetricKey := metrics.BERequestMetricsWordsRouteKey
// 	defer metrics.SendDuration(beRequestMetricKey, time.Now())
// 	metrics.IncrementRequestCounter(beRequestMetricKey)

// 	words, ok := service.GetAllWords()
// 	if !ok {
// 		logrus.Error("Unable to get all words from DB")
// 		http.Error(w, "Unable to get all words from DB", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	mappedWords := service.MapWordsToUi(words)

// 	result, err := json.Marshal(mappedWords)

// 	if err != nil {
// 		logrus.Error(err, "Unable to marshal get all words response")
// 		http.Error(w, "Unable to marshal get all words response", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}

// 	w.Header().Set(common.ContentType, common.ApplicationJSON)
// 	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
// 	w.WriteHeader(http.StatusOK)
// 	_, err = fmt.Fprintf(w, "%s", result)

// 	if err != nil {
// 		logrus.Error(err, "Unable to write get words response in response writer")
// 		http.Error(w, "Unable to write get words response in response writer", http.StatusInternalServerError)
// 		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
// 		return
// 	}
// }

func UpdateWords(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsWordsRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("unsupported media type to patch words")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error(err, "Unable to read patch words request body")
		http.Error(w, "Unable to read patch words request body", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	var words []incoming.UpdateWordFromUI
	err = json.Unmarshal(body, &words)
	if err != nil {
		logrus.Error(err, "Unable to unmarshal patch words request body")
		http.Error(w, "Unable to unmarshal patch words request body", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	for _, bodyWord := range words {
		word := service.MapWordToDBModel(&bodyWord)
		resp, ok := word.Create()
		if !ok {
			logrus.Error("Unable to patch word in DB", resp)
			http.Error(w, "Unable to patch word in DB", http.StatusBadRequest)
			metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
			return
		}
		logrus.Info("Word patched ", resp)
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
}

func DeleteWords(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsWordsRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("unsupported media type to delete words")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error(err, "Unable to read delete words request body")
		http.Error(w, "Unable to read delete words request body", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	var words []incoming.UpdateWordFromUI
	err = json.Unmarshal(body, &words)
	if err != nil {
		logrus.Error(err, "Unable to unmarshal delete words request body")
		http.Error(w, "Unable to unmarshal delete words request body", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	for _, bodyWord := range words {
		word := service.MapWordToDBModel(&bodyWord)
		resp, ok := word.Delete()
		if !ok {
			logrus.Error("Unable to delete word from DB", word)
			http.Error(w, "Unable to delete word from DB", http.StatusBadRequest)
			metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
			return
		}
		logrus.Info("Word deleted: ", resp)
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
}

func CreateWords(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestMetricsWordsRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("unsupported media type to post words")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error(err, "Unable to read create words from raw request body")
		http.Error(w, "Unable to read create words from raw request body", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	var words []incoming.NewWordFromUI
	err = json.Unmarshal(body, &words)
	if err != nil {
		logrus.Error(err, "Unable to unmarshal create words from raw request body")
		http.Error(w, "Unable to unmarshal create words from raw request body", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	mappedWords := service.MapNewWords(words)

	for _, word := range mappedWords {
		resp, ok := word.Create()
		if !ok {
			logrus.Error("Unable to create word from raw in DB: ", resp)
			http.Error(w, "Unable to create word from raw in DB", http.StatusBadRequest)
			metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
			return
		}
		logrus.Info("Word from raw is created ", resp)
	}

	w.Header().Set(common.ContentType, common.ApplicationJSON)
	w.Header().Set(common.AccessControlAllowOrigin, common.Asterisk)
	w.WriteHeader(http.StatusOK)
}

func PostEvents(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestPostEventsRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)

	contentType := r.Header.Get(common.ContentType)
	if contentType != common.ApplicationJSON {
		err := errors.New("unsupported media type of post events")
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusUnsupportedMediaType)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusUnsupportedMediaType)
		return
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		logrus.Error(err, "Unable to read UI events request body")
		http.Error(w, "Unable to read UI event request body", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	var events []metrics.UIEvent
	err = json.Unmarshal(body, &events)
	if err != nil {
		logrus.Error(err, "Unable to unmarshal UI events request body")
		http.Error(w, "Unable to unmarshal UI event request body", http.StatusBadRequest)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusBadRequest)
		return
	}

	err = metrics.SendUIEvent(events[0])
	if err != nil {
		logrus.Error(err, "Unable to send UI events to Prometheus")
		http.Error(w, "Unable to send UI events to Prometheus", http.StatusInternalServerError)
		metrics.IncrementErrorResponseCounter(beRequestMetricKey, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func GetMetrics(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	beRequestMetricKey := metrics.BERequestGetMetricsRouteKey
	defer metrics.SendDuration(beRequestMetricKey, time.Now())
	metrics.IncrementRequestCounter(beRequestMetricKey)
	promhttp.Handler().ServeHTTP(w, r)
}
